﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinInPlace : MonoBehaviour
{

    [SerializeField]
    private Vector3 rotSpeed = new Vector3(50f,50f,50f);

    void FixedUpdate()
    {
        transform.Rotate(rotSpeed * Time.deltaTime);
    }

}
